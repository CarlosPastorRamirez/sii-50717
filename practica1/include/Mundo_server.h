// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include "Vector2D.h"
#include "logger.h"
#include "fifo_server.h"
#include <pthread.h>
#include "Socket.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemComp.h"


class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();
	void OnTimer(int value);
	void OnDraw();
	void RecibeComandosJugador1();
	void RecibeComandosJugador2();
	void GestionaConexiones();

	char status[200];
	char player_key[200];

	int flagEnd;

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	
	bool flag_coenxion_1;
	bool flag_coenxion_2;
	bool flag_processing_accp;

	//fifos
	Logger log;

	int fd;
	//Puntuaciones
	int puntos1;
	int puntos2;
	
	//cuenta de rebotes
	int cont;

	//tread
	pthread_t trd1;
	pthread_t trd2;
	pthread_t cnnct_trd;

	//Socket
	Socket cnnct;
	std::vector<Socket> clnts;

};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
