#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>

class fifo_server{
public:
	int fd_sr_cl; 
	int fd_cl_sr;

/////////////////////////CONSTRUCTOR///////////////////////////////////77
	fifo_server(){
		if (mkfifo("/tmp/sr-cl",0700)!=0) 
		{
			char *s;
			perror(s);
			exit(1);
		}
		if (mkfifo("/tmp/cl-sr",0700)!=0) 
		{
			char *s;
			perror(s);
			exit(1);
		}
	
		printf("exito 1\n");

		fd_cl_sr = open("/tmp/cl-sr", O_RDONLY);
		fd_sr_cl = open("/tmp/sr-cl", O_WRONLY);
	
		printf("exito 2\n");
	}
/////////////////////METODOS LECTURA ESCRITURA/////////////////////////////
	void read_PIPE(char *buff){
		if(read(fd_cl_sr, buff, 200*sizeof(char)) < 0) {
			perror("Read the pipe");
	    		exit(1);
		}
	}
	void write_PIPE(const char *buff)
	{
		if(write(fd_sr_cl, buff, 200*sizeof(char)) < 0)
		{
			perror("write the pipe");
	    		exit(1);
		}
	}
/////////////////////////////////////////////////////////////////////////

	~fifo_server()
	{
		close(fd_cl_sr);
		unlink("/tmp/cl-sr");
		close(fd_sr_cl);
		unlink("/tmp/sr-cl");
	}

};
