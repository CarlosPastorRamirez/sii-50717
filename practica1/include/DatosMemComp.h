#pragma once 

#include "Esfera.h"
#include "Raqueta.h"

#include <unistd.h>
#include "sys/mman.h"

class DatosMemComp
{       
public:         
	Esfera esfera;
	Raqueta raqueta1;
 	int accion; //1 arriba, 0 nada, -1 abajo
	int endFlag;
};
