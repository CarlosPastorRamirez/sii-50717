#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>

class fifo_client{
public:
	int fd_sr_cl;
	int fd_cl_sr;

	fifo_client()
	{

	printf("exito\n");

	fd_cl_sr = open("/tmp/cl-sr", O_WRONLY);
	fd_sr_cl = open("/tmp/sr-cl", O_RDONLY);
	
	printf("fifoinit\n");

	}
/////////////////////METODOS LECTURA ESCRITURA/////////////////////////////
	void read_PIPE(char *buff){
		if(read(fd_sr_cl, buff, 200*sizeof(char)) < 0) {
			perror("Read the pipe");
	    		exit(2);
		}
	}
	void write_PIPE(char *buff){
		if(write(fd_cl_sr, buff, 200*sizeof(char))<0)
		{
			perror("write the pipe");
	    		exit(2);
		}
	}
/////////////////////////////////////////////////////////////////////////
	~fifo_client()
	{
		close(fd_cl_sr);
		unlink("/tmp/cl-sr");
		close(fd_sr_cl);
		unlink("/tmp/sr-cl");
	}
};
