#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>

#include <math.h>

#include "DatosMemComp.h"

int main(){

	DatosMemComp * dbot;
	int fd, acc;

	fd = open("/tmp/pro_mem", O_CREAT^O_RDWR, 0777);
	
	ftruncate(fd, sizeof(*dbot));

	dbot = (DatosMemComp *)mmap(NULL, sizeof(*dbot), PROT_WRITE^PROT_READ, MAP_SHARED, fd, 0);

	dbot->endFlag = 0;

	for(;;){	
	
		if(dbot->esfera.centro.y > dbot->raqueta1.centro) acc = 1;
		else if(dbot->esfera.centro.y < dbot->raqueta1.centro) acc = -1;
		else acc = 0;
		dbot->accion = acc;
		if(dbot->endFlag == 1){
			close(fd);
			unlink("/tmp/pro_mem");
			return 0;
		}
		usleep(25000);
	}
}
