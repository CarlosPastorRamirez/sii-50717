// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "glut.h"
#include "Mundo_server.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"SERVIDOR");
	print(cad,350,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	if(flag_processing_accp == false)
	{
		jugador1.Mueve(0.025f);
		jugador2.Mueve(0.025f);

		esfera.Mueve(0.025f);

		int i;
		for(i=0;i<paredes.size();i++)
		{
			paredes[i].Rebota(esfera);
			paredes[i].Rebota(jugador1);
			paredes[i].Rebota(jugador2);
		}

		if( jugador1.Rebota(esfera) || jugador2.Rebota(esfera)) cont++;

		if( fondo_izq.Rebota(esfera) )
		{
			esfera.centro.x=0;
			esfera.centro.y=rand()/(float)RAND_MAX;
			esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
			esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos2++;
			log.write_L("2");
			printf("j1:%d\n", puntos2);
			usleep(1000);
		}

		if( fondo_dcho.Rebota(esfera) )
		{
			esfera.centro.x=0;
			esfera.centro.y=rand()/(float)RAND_MAX;
			esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
			esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
			puntos1++;
			log.write_L("1");
			printf("j1:%d\n", puntos1);
			usleep(1000);
		}

		if(cont%6 == 0)
		{
			if(esfera.getRad()>0.1) esfera.setRad(esfera.getRad()/2);
			cont = 1;
		}

		printf("sending\n");
	
		for(int i=0; i < clnts.size(); i++)
		{
		sprintf(status,"%f %f %f %f %f %f %f %f %f %f %f %f %d %d %f %i\n", esfera.centro.x,esfera.centro.y,jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2,jugador1.centro,jugador2.centro, puntos1, puntos2, esfera.radio, i); 
		clnts[i].Send(status, 200*sizeof(char));
		}
		printf("sent\n");

		if(puntos1 == 3 || puntos2 == 3){
			printf("fin\n");
			usleep(100000);
			log.write_L("F");
			if(flagEnd == 1)
			usleep(100000);
			exit(1);
		}

	}

}

void* hilo_comandos1(void* d)
{
	CMundo* p=(CMundo*) d;
	p->RecibeComandosJugador1();
	printf("dn_hcom\n");
}
void* hilo_comandos2(void* d)
{
	CMundo* p=(CMundo*) d;
	p->RecibeComandosJugador2();
	printf("dn_hcom\n");
}

void* hilo_conexiones(void* d)
{
	CMundo* p=(CMundo*) d;
	p->GestionaConexiones();
	printf("dn_hcon\n");
}

void CMundo::GestionaConexiones() {

	while (1) {

		printf("AC\n");

		Socket skt = cnnct.Accept();

		printf("ACTD\n");		

		flag_processing_accp = true;

		clnts.push_back(skt);
		printf("Conexion acceptada\n");
		clnts[clnts.size() -1].Receive(status, 200*sizeof(char));
		printf("Recibido\n");
		printf("%s\n", status);
		printf("Nombre cliente\n");
			if(clnts.size() == 1) flag_coenxion_1 = true;
			if(clnts.size() == 2) {
				flag_coenxion_2 = true;
			//a la izq
				jugador1.g=0;
				jugador1.x1=-6;jugador1.y1=-1;
				jugador1.x2=-6;jugador1.y2=1;
			//a la dcha
				jugador2.g=0;
				jugador2.x1=6;jugador2.y1=-1;
				jugador2.x2=6;jugador2.y2=1;
			//Reiniciar esfera
				esfera.centro.x=0;
				esfera.centro.y=rand()/(float)RAND_MAX;
				esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
				esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
			//Reiniciar puntuacion
				puntos1 = 0;
				puntos2 = 0;
			}

		flag_processing_accp = false;
	}
}

void CMundo::RecibeComandosJugador1()
{
	while (1) {
		usleep(10);
		clnts[0].Receive(player_key, 3*sizeof(char));
		unsigned char key;
		if(player_key[0]=='s')jugador1.velocidad.y=-4;
		if(player_key[0]=='w')jugador1.velocidad.y=4;
      }
}

void CMundo::RecibeComandosJugador2()
{
	while (1) {
		usleep(10);
		if(clnts.size() == 2) clnts[1].Receive(player_key, 3*sizeof(char));
		unsigned char key;
		if(player_key[1]=='l')jugador2.velocidad.y=-4;
		if(player_key[1]=='o')jugador2.velocidad.y=4;
      }
}

void CMundo::Init()
{
	cont = 1;
	Plano p;

//paredes

	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//puntos
	puntos1 = 0;
	puntos2 = 0;

	//flags
	flag_coenxion_1 = false;
	flag_coenxion_2 = false;
	flag_processing_accp = false;

	//flagEnd
	flagEnd = 0;

	//Socket
	cnnct.InitServer("192.168.124.139", 2100);
	usleep(1000000);


	//inicializacion de treads

	printf("tr_con\n");	
	pthread_create(&cnnct_trd, NULL, hilo_conexiones, this);

	printf("wa\n");	
	while(flag_coenxion_1 == false) printf("dnalop\n");

	printf("tr_com\n");
	pthread_create(&trd1, NULL, hilo_comandos1, this);
	pthread_create(&trd2, NULL, hilo_comandos2, this);

}
