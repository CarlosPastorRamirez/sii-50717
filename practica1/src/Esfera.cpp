// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio = 0.5f;
	velocidad.x=3;
	velocidad.y=3;
}
Esfera::Esfera(Vector2D pos, Vector2D vel, float rad)
{
	radio=rad;
	centro.x = pos.x;
	centro.y = pos.y;
	velocidad.x = vel.x;
	velocidad.y = vel.y;
}

void Esfera::setRad(float rad)
{
	radio = rad;
}
Vector2D Esfera::getPos()
{
	return centro;
}
Vector2D Esfera::getVel()
{
	return velocidad;
}
float Esfera::getRad()
{
	return radio;
}
Esfera::~Esfera()
{

}

void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro.x = centro.x + velocidad.x *t;
	centro.y = centro.y + velocidad.y *t;
}
